webpackJsonp([0],{

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShortlistedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__detail_detail__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chatlist_chatlist__ = __webpack_require__(56);





var ShortlistedPage = /** @class */ (function () {
    function ShortlistedPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ShortlistedPage.prototype.detail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__detail_detail__["a" /* DetailPage */]);
    };
    ShortlistedPage.prototype.chatlist = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chatlist_chatlist__["a" /* ChatlistPage */]);
    };
    ShortlistedPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-shortlisted',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\shortlisted\shortlisted.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Shortlisted</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button clear (click)="chatlist()">\n\n            <ion-icon name="md-chatbubbles"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\shortlisted\shortlisted.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], ShortlistedPage);
    return ShortlistedPage;
}());

//# sourceMappingURL=shortlisted.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.detail = false;
    }
    ChatPage.prototype.showDetail = function () {
        this.detail = !this.detail;
        console.log(this.detail);
    };
    ChatPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\chat\chat.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-list no-margin no-lines no-padding>\n\n            <ion-item no-padding>\n\n                <ion-avatar item-start>\n\n                    <img src="assets/imgs/Layer30.png" />\n\n                </ion-avatar>\n\n                <h2>Johan deo</h2>\n\n            </ion-item>\n\n        </ion-list>\n\n        <ion-buttons end>\n\n            <button ion-button clear padding (click)="showDetail()">\n\n                <ion-icon *ngIf="detail" name="ios-arrow-up"></ion-icon>\n\n                <ion-icon *ngIf="!detail" name="ios-arrow-down"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <div [ngClass]="detail ? \'few-detail show\' : \'few-detail\'">\n\n        <ion-list>\n\n            <ion-item>\n\n                <ion-thumbnail item-start>\n\n                    <img src="assets/imgs/Layer4.png" />\n\n                </ion-thumbnail>\n\n                <h2>Alise Altezza</h2>\n\n                <p>Near worli sea link, worli, Mumbai.</p>\n\n                <ion-row>\n\n                    <ion-col col-6>\n\n                        <h4>$750k</h4>\n\n                    </ion-col>\n\n                    <ion-col col-6 text-right>\n\n                        <h4 class="small">View more</h4>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n    <div class="chat-board">\n\n        <ul>\n\n            <li>\n\n                <div class="send">\n\n                    <p>Hellow! I am interested in this property.</p>\n\n                    <span class="time">1:24 pm</span>\n\n                </div>\n\n            </li>\n\n            <li>\n\n                <div class="recive">\n\n                    <p>Hey! I really appreciate yourchoice of home.</p>\n\n                    <span class="time">1:25 pm</span>\n\n                </div>\n\n            </li>\n\n            <li>\n\n                <div class="send">\n\n                    <div class="img">\n\n                        <img src="assets/imgs/Layer23.png" />\n\n                    </div>\n\n                    <p>Really like the parking space at front.</p>\n\n                    <span class="time">1:26 pm</span>\n\n                </div>\n\n            </li>\n\n        </ul>\n\n    </div>\n\n\n\n    <ion-list class="footer">\n\n        <ion-item>\n\n            <ion-input type="text" value="" placeholder="Write your message"></ion-input>\n\n            <ion-buttons item-end>\n\n                <button ion-button clear>\n\n                    <ion-icon name="md-attach"></ion-icon>\n\n                </button>\n\n                <button ion-button clear>\n\n                    <ion-icon name="md-happy"></ion-icon>\n\n                </button>\n\n                <button ion-button clear>\n\n                    <ion-icon name="md-send"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\chat\chat.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RateusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var RateusPage = /** @class */ (function () {
    function RateusPage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
    }
    RateusPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    RateusPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-rateus',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\rateus\rateus.html"*/'<!--\n\n<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Rate us</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n-->\n\n\n\n<ion-content padding class="bg-light">\n\n    <ion-card text-center>\n\n        <ion-card-header>\n\n            Rate us\n\n        </ion-card-header>\n\n        <ion-card-content>\n\n            <p>Hey.Love us? Express it!</p>\n\n            <div class="star">\n\n                <ion-icon name="md-star"></ion-icon>\n\n                <ion-icon name="md-star"></ion-icon>\n\n                <ion-icon name="md-star"></ion-icon>\n\n                <ion-icon name="md-star"></ion-icon>\n\n                <ion-icon name="md-star"></ion-icon>\n\n            </div>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <button ion-button col block class="light" (click)="close()">Remind Later</button>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <button ion-button col block>Rate it</button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\rateus\rateus.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */]])
    ], RateusPage);
    return RateusPage;
}());

//# sourceMappingURL=rateus.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__password_password__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(268);





var SigninPage = /** @class */ (function () {
    function SigninPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    SigninPage.prototype.password = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__password_password__["a" /* PasswordPage */]);
    };
    SigninPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    SigninPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-signin',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\signin\signin.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title></ion-title>\n\n        <ion-buttons end margin-left margin-right>\n\n            <button ion-button clear no-padding>\n\n            Help?\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <figure class="home-img" text-center>\n\n        <img src="assets/imgs/home.png">\n\n    </figure>\n\n    <div class="detail">\n\n        <h4 text-center>Enter your phone number to showcase of properties</h4>\n\n        <p text-center>It will take less than a minutes</p>\n\n    </div>\n\n\n\n    <ion-list no-lines class="form">\n\n        <ion-item>\n\n            <ion-input type="number" placeholder="Your Phone Number"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <button ion-button block (click)="password()">Next</button>\n\n    <button ion-button block outline (click)="register()">New user?</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\signin\signin.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */]])
    ], SigninPage);
    return SigninPage;
}());

//# sourceMappingURL=signin.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);





var VerificationPage = /** @class */ (function () {
    //let verificationId;
    function VerificationPage(navCtrl, menuCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.code = "";
        //OTP values
        this.value1 = "";
        this.value2 = "";
        this.value3 = "";
        this.value4 = "";
        this.value5 = "";
        this.value6 = "";
        this.menuCtrl.enable(false, 'myMenu');
        this.fullname = navParams.get('fullname');
        this.phonenumber = navParams.get('phonenumber');
        this.user = navParams.get('user');
        console.log("this.user->" + this.user);
        //verify phone number
        window.FirebasePlugin.verifyPhoneNumber(this.phonenumber, 60, function (credential) {
            //sms sent checked
            console.log("credential:" + credential);
            // console.log(credential);
            //this.code = credential.instantVerification ? credential.code : inputField.value.toString();
            console.log("credential.verificationId->" + credential.verificationId);
            var verificationIdn = credential.verificationId;
            _this.verificationId = verificationIdn;
            console.log("this.verificationId ->" + _this.verificationId);
            //this.navCtrl.push(VerificationPage,{fullname:this.fullname,phonenumber:this.phonenumber});
        }, function (error) {
            console.log("**error:**" + error);
        });
        //this.verificationId = navParams.get('vid');
    }
    VerificationPage.prototype.home = function (value1, value2, value3, value4, value5, value6) {
        var _this = this;
        this.value1 = value1.value;
        this.value2 = value2.value;
        this.value3 = value3.value;
        this.value4 = value4.value;
        this.value5 = value5.value;
        this.value6 = value6.value;
        console.log("this.value1->", this.value1);
        console.log("this.value2->", this.value2);
        console.log("this.value3->", this.value3);
        console.log("this.value4->", this.value4);
        console.log("this.value5->", this.value5);
        console.log("this.value6->", this.value6);
        this.code = this.value1 + this.value2 + this.value3 + this.value4 + this.value5 + this.value6;
        console.log("this.code->" + this.code);
        var cred = __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"].PhoneAuthProvider.credential(this.verificationId, this.code);
        __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().signInWithCredential(cred).then(function () {
            //navigate to homepage for successful login
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */], { user: _this.user });
        }, function (reason) {
            console.log(reason);
        });
        //
        // (<any>window).FirebasePlugin.verifyPhoneNumber(this.phonenumber, 60, function(credential) {
        //
        //   //sms sent checked
        //   console.log("credential:" + credential);
        //
        //   // console.log(credential);
        //    //this.code = credential.instantVerification ? credential.code : inputField.value.toString();
        //   console.log("credential.verificationId->"+credential.verificationId);
        //   let verificationId = credential.verificationId;
        //   console.log("this.verificationId ->" + verificationId);
        //
        //
        //
        //   let cred = firebase.auth.PhoneAuthProvider.credential(verificationId, this.code);
        //
        //   //window.prompt('credential ->' + credential);
        //
        //   firebase.auth().signInWithCredential(cred).then(() => {
        //
        //     //navigate to homepage for successful login
        //       this.navCtrl.setRoot(HomePage);
        //
        //   }, reason => {
        //      console.log(reason);
        //   });
        //
        //   // sign in with the credential
        //   //firebase.auth().signInWithCredential(credential);
        //
        //   // this.verificationID = credential.verificationId ; //save credential id
        //  }, error => {
        //    console.log("**error:**" + error);
        //  });
        //
        //
        //this.navCtrl.setRoot(HomePage);
    };
    VerificationPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-verification',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\verification\verification.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title></ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n\n\n    <h4 text-center>Verify your number, {{fullname}}</h4>\n\n\n\n    <p text-center>Enter 6 digit verification code sent on your number {{phonenumber}}</p>\n\n\n\n    <!-- <ion-input type="text" placeholder="Verification code" [(ngModel)]="code"></ion-input> -->\n\n     <ion-list no-lines class="form row">\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value1  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value2  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value3  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value4  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value5  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n        <ion-item col-2>\n\n            <ion-input type="tel" #value6  text-center maxlength="1"></ion-input>\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n    <button ion-button block (click)="home(value1,value2,value3,value4,value5,value6)">Verify Number</button>\n\n    <ion-item no-padding no-lines class="text">\n\n        01:05 min left\n\n        <ion-buttons item-end style="margin-right: 0;">Resend</ion-buttons>\n\n    </ion-item>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\verification\verification.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */]])
    ], VerificationPage);
    return VerificationPage;
}());

//# sourceMappingURL=verification.js.map

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 148;

/***/ }),

/***/ 192:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 192;

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var SettingPage = /** @class */ (function () {
    function SettingPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SettingPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\setting\setting.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Settings</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list radio-group no-lines>\n\n        <ion-list-header>\n\n            Manage Notification\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-label>For Matched Properties</ion-label>\n\n            <ion-toggle checked="true"></ion-toggle>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>For New Launched Projects</ion-label>\n\n            <ion-toggle checked="false"></ion-toggle>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Latest Property News</ion-label>\n\n            <ion-toggle checked="false"></ion-toggle>\n\n        </ion-item>\n\n    </ion-list>\n\n    <ion-list radio-group no-lines>\n\n        <ion-list-header>\n\n            Frequency of Notification\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-label>Daily</ion-label>\n\n            <ion-radio value="Daily"></ion-radio>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Alternate Day</ion-label>\n\n            <ion-radio value="Alternate" checked="true"></ion-radio>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Weekly</ion-label>\n\n            <ion-radio value="Weekly"></ion-radio>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\setting\setting.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TncPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var TncPage = /** @class */ (function () {
    function TncPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    TncPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-tnc',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\tnc\tnc.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Terms & condition</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <div class="tnc">\n\n        <h2>Condition of use</h2>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text. Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n\n\n        <h2>Privacy</h2>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text. Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n\n\n        <h2>Terms of documents</h2>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text. Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\tnc\tnc.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], TncPage);
    return TncPage;
}());

//# sourceMappingURL=tnc.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contact_contact__ = __webpack_require__(70);




var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage.prototype.contact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__contact_contact__["a" /* ContactPage */]);
    };
    AboutPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\about\about.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>About us</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <div class="header-logo">\n\n        <figure>\n\n            <img src="assets/imgs/logo.png">\n\n        </figure>\n\n        <ion-icon name="md-call" (click)="contact()"></ion-icon>\n\n    </div>\n\n    <div class="about-text">\n\n        <h2>About Proprty Hub</h2>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n        <p>Multi-line lists are identical to regular lists, except items have multiple lines of text. Multi-line lists are identical to regular lists, except items have multiple lines of text.</p>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\about\about.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profileedit_profileedit__ = __webpack_require__(239);




var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProfilePage.prototype.profileedit = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__profileedit_profileedit__["a" /* ProfileeditPage */]);
    };
    ProfilePage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\profile\profile.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Profile</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <div class="profile-img">\n\n        <img src="assets/imgs/Layer30.png">\n\n        <h1>{{name}}</h1>\n\n        <ion-icon name="md-create" (click)="profileedit()"></ion-icon>\n\n    </div>\n\n    <ion-list class="contact" no-lines>\n\n        <ion-item>\n\n            <ion-label stacked>Email Address</ion-label>\n\n            <ion-input type="email" value="johandeo123@gmail.com" disabled></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label stacked>Mobile Number</ion-label>\n\n            <ion-input type="text" value="+91 9087654321" disabled></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label stacked>Residence City</ion-label>\n\n            <ion-input type="text" value="Andheri (E), Maharastra" disabled></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\profile\profile.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileeditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var ProfileeditPage = /** @class */ (function () {
    function ProfileeditPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ProfileeditPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-profileedit',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\profileedit\profileedit.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Edit Prifile</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <ion-list no-lines class="form-profile">\n\n        <ion-item>\n\n            <ion-thumbnail item-start>\n\n                <ion-icon name="md-camera"></ion-icon>\n\n                <img src="assets/imgs/Layer30.png">\n\n            </ion-thumbnail>\n\n            <ion-label stacked>Your Name</ion-label>\n\n            <ion-input type="text" value="Johan Deo"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label stacked>Email address</ion-label>\n\n            <ion-input type="email" value="johandeo12@gmail.com"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label stacked>Mobile Number</ion-label>\n\n            <ion-input type="text" value="+91 9876543210"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label stacked>Residence City</ion-label>\n\n            <ion-input type="text" value="Andheri (E), Maharastra"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <button ion-button block>Update Profile</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\profileedit\profileedit.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], ProfileeditPage);
    return ProfileeditPage;
}());

//# sourceMappingURL=profileedit.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__result_result__ = __webpack_require__(241);




var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SearchPage.prototype.result = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__result_result__["a" /* ResultPage */]);
    };
    SearchPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\search\search.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add location</ion-title>\n\n        <ion-buttons end margin-left margin-right>\n\n            <button ion-button clear no-padding>\n\n            Reset\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <!--    <ion-searchbar placeholder="Search Location"></ion-searchbar>-->\n\n    <ion-list>\n\n        <ion-item no-padding>\n\n            <!--            <ion-label floating>Search Location</ion-label>-->\n\n            <ion-input type="text" placeholder="Search Location"></ion-input>\n\n            <ion-buttons item-end margin-left margin-right>\n\n                <ion-icon name="md-locate"></ion-icon>\n\n            </ion-buttons>\n\n        </ion-item>\n\n    </ion-list>\n\n    <h6>Popular Location</h6>\n\n    <div class="option" padding-bottom>\n\n        <button ion-button small class="active">Worli</button>\n\n        <button ion-button small class="active">Bandra</button>\n\n        <button ion-button small>Andheri</button>\n\n        <button ion-button small>Borivali</button>\n\n        <button ion-button small class="active">Jogeshwari</button>\n\n        <button ion-button small>Juhu</button>\n\n    </div>\n\n\n\n    <button ion-button block (click)="result()">Search</button>\n\n    <div class="" style="margin-left: -16px;margin-right: -16px;">\n\n        <ion-card class="items-div" (click)="detail()">\n\n            <ion-icon name="md-heart" class="like"></ion-icon>\n\n            <ion-scroll scrollX class="scrol-items-div">\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer6.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer23.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer2.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer4.png" />\n\n                </div>\n\n            </ion-scroll>\n\n\n\n            <ion-card-content>\n\n                <ion-list>\n\n                    <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n                </ion-list>\n\n            </ion-card-content>\n\n        </ion-card>\n\n        <ion-card class="items-div" (click)="detail()">\n\n            <ion-icon name="md-heart" class="like"></ion-icon>\n\n            <ion-scroll scrollX class="scrol-items-div">\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer4.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer2.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer23.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer6.png" />\n\n                </div>\n\n            </ion-scroll>\n\n\n\n            <ion-card-content>\n\n                <ion-list>\n\n                    <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n                </ion-list>\n\n            </ion-card-content>\n\n        </ion-card>\n\n        <ion-card class="items-div" (click)="detail()">\n\n            <ion-icon name="md-heart" class="like"></ion-icon>\n\n            <ion-scroll scrollX class="scrol-items-div">\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer6.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer23.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer2.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer4.png" />\n\n                </div>\n\n            </ion-scroll>\n\n\n\n            <ion-card-content>\n\n                <ion-list>\n\n                    <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n                </ion-list>\n\n            </ion-card-content>\n\n        </ion-card>\n\n        <ion-card class="items-div" (click)="detail()">\n\n            <ion-icon name="md-heart" class="like"></ion-icon>\n\n            <ion-scroll scrollX class="scrol-items-div">\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer4.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer2.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer23.png" />\n\n                </div>\n\n                <div class="scroll-item">\n\n                    <img src="assets/imgs/Layer6.png" />\n\n                </div>\n\n            </ion-scroll>\n\n\n\n            <ion-card-content>\n\n                <ion-list>\n\n                    <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n                </ion-list>\n\n            </ion-card-content>\n\n        </ion-card>\n\n    </div>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\search\search.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filters_filters__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chatlist_chatlist__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_detail__ = __webpack_require__(69);






var ResultPage = /** @class */ (function () {
    function ResultPage(navCtrl, modalCtrl, actionsheetCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.actionsheetCtrl = actionsheetCtrl;
    }
    //filters() {
    // const modal = this.modalCtrl.create(FiltersPage);
    // modal.present();
    //}
    ResultPage.prototype.filters = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__filters_filters__["a" /* FiltersPage */]);
    };
    ResultPage.prototype.chatlist = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chatlist_chatlist__["a" /* ChatlistPage */]);
    };
    ResultPage.prototype.detail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_detail__["a" /* DetailPage */]);
    };
    ResultPage.prototype.openMenu = function () {
        var actionSheet = this.actionsheetCtrl.create({
            title: 'Sort By',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Relevance',
                },
                {
                    text: 'Most Recent',
                },
                {
                    text: 'Price: Low to High',
                },
                {
                    text: 'Price: Low to High',
                }
            ]
        });
        actionSheet.present();
    };
    ResultPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-result',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\result\result.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Sale\n\n            <p no-margin ion-text class="small">330 properties</p>\n\n        </ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button clear (click)="chatlist()">\n\n            <ion-icon name="md-chatbubbles"></ion-icon>\n\n            </button>\n\n            <button ion-button clear>\n\n                <ion-icon name="md-map"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-row>\n\n        <ion-col (click)="openMenu()" class="border">\n\n            <button ion-button full clear>\n\n                <ion-icon name="md-swap" margin-right></ion-icon>\n\n              Sort\n\n            </button>\n\n        </ion-col>\n\n        <ion-col (click)="filters()">\n\n            <button ion-button full clear>\n\n                <ion-icon name="md-options" margin-right></ion-icon>\n\n              Filter\n\n            </button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card class="items-div" (click)="detail()">\n\n        <ion-icon name="md-heart" class="like"></ion-icon>\n\n        <ion-scroll scrollX class="scrol-items-div">\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </div>\n\n            <div class="scroll-item">\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </div>\n\n        </ion-scroll>\n\n\n\n        <ion-card-content>\n\n            <ion-list>\n\n                <button ion-item no-padding>\n\n                            Alise Altezza<br>\n\n                            <small>Near worli sea link, worli, Mumbai.</small>\n\n                            <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                        </button>\n\n            </ion-list>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <div class="no-data" text-center style="display: none;">\n\n        <figure>\n\n            <img src="assets/imgs/placeholder.png">\n\n        </figure>\n\n        <h6>Oops ! No properties to show</h6>\n\n        <p>Set alert for property in area you\'ve selected. We\'ll notify you when any property will available in this area.</p>\n\n        <strong>Notify Me</strong>\n\n\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\result\result.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* ActionSheetController */]])
    ], ResultPage);
    return ResultPage;
}());

//# sourceMappingURL=result.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var FiltersPage = /** @class */ (function () {
    function FiltersPage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.structure = { lower: 33, upper: 60 };
        this.gaming1 = "p1";
        this.gaming2 = "b1";
        this.gaming3 = "b5";
    }
    FiltersPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-filters',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\filters\filters.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title end>Filters</ion-title>\n\n        <ion-buttons end margin-left>\n\n            <button ion-button clear>\n\n            Reset\n\n            </button>\n\n            <!--\n\n<button (click)="close()" ion-button clear>\n\n            Close\n\n            </button>\n\n-->\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <ion-list no-lines>\n\n        <ion-list-header>\n\n            Location\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-input type="text" value="" placeholder="add more"></ion-input>\n\n            <ion-buttons item-end margin-left margin-right>\n\n                <ion-icon name="md-locate"></ion-icon>\n\n            </ion-buttons>\n\n        </ion-item>\n\n        <ion-list-header>\n\n            Property Type\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-select [(ngModel)]="gaming1" block>\n\n                <ion-option value="p1">Appartment & Unit</ion-option>\n\n                <ion-option value="p2">Appartment</ion-option>\n\n                <ion-option value="p3">Unit</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-list-header>\n\n            Budget\n\n        </ion-list-header>\n\n        <ion-item>\n\n            <ion-item>\n\n                <ion-range dualKnobs="true" pin="true" [(ngModel)]="structure" color="dark">\n\n                </ion-range>\n\n            </ion-item>\n\n        </ion-item>\n\n        <ion-list-header>\n\n            Bedrooms\n\n        </ion-list-header>\n\n        <ion-row>\n\n            <ion-col col-6>\n\n                <ion-item>\n\n                    <ion-label>Min</ion-label>\n\n                    <ion-select [(ngModel)]="gaming2" block>\n\n                        <ion-option value="b1">1</ion-option>\n\n                        <ion-option value="b2">2</ion-option>\n\n                        <ion-option value="b3">3</ion-option>\n\n                        <ion-option value="b4">4</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n            </ion-col>\n\n            <ion-col col-6>\n\n                <ion-item>\n\n                    <ion-label>Max</ion-label>\n\n                    <ion-select [(ngModel)]="gaming3" block>\n\n                        <ion-option value="b5">2</ion-option>\n\n                        <ion-option value="b6">3</ion-option>\n\n                        <ion-option value="b7">4</ion-option>\n\n                        <ion-option value="b8">5</ion-option>\n\n                        <ion-option value="b9">6</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-list>\n\n    <button ion-button block>Find 300+ Properties</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\filters\filters.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */]])
    ], FiltersPage);
    return FiltersPage;
}());

//# sourceMappingURL=filters.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var NotificationPage = /** @class */ (function () {
    function NotificationPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    NotificationPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\notification\notification.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Notification</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <ion-list no-margin no-lines>\n\n        <ion-item>\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer2.png" />\n\n            </ion-avatar>\n\n            <h2>New property in your area</h2>\n\n            <p>Near Worli Sea Link, Worli, Mumbai.</p>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer4.png" />\n\n            </ion-avatar>\n\n            <h2>Hurry ! Book with 5 Lakh.</h2>\n\n            <p>Near Worli Sea Link, Worli, Mumbai.</p>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer6.png" />\n\n            </ion-avatar>\n\n            <h2>New property in your area</h2>\n\n            <p>Near Worli Sea Link, Worli, Mumbai.</p>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer23.png" />\n\n            </ion-avatar>\n\n            <h2>Hurry ! Book with 5 Lakh.</h2>\n\n            <p>Near Worli Sea Link, Worli, Mumbai.</p>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\notification\notification.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__fogotpassword_fogotpassword__ = __webpack_require__(265);





var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    PasswordPage.prototype.home = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    PasswordPage.prototype.fogotpassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__fogotpassword_fogotpassword__["a" /* FogotpasswordPage */]);
    };
    PasswordPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\password\password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-buttons end margin-left margin-right>\n\n            <button ion-button clear no-padding>\n\n            Help?\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <figure class="home-img" text-center>\n\n        <img src="assets/imgs/home.png">\n\n    </figure>\n\n    <div class="detail">\n\n        <h4 text-center>Hey Johan ! you\'re already registered with us.</h4>\n\n        <p text-center>Enter your password to continue.</p>\n\n    </div>\n\n\n\n    <ion-list no-lines class="form">\n\n        <ion-item>\n\n            <ion-input type="number" placeholder="Enter your 6 digit password"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <button ion-button block (click)="home()">Sign in</button>\n\n    <button ion-button block outline (click)="fogotpassword()">Forgot Password?</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\password\password.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FogotpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__verification_verification__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signin_signin__ = __webpack_require__(137);





var FogotpasswordPage = /** @class */ (function () {
    function FogotpasswordPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    FogotpasswordPage.prototype.verification = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__verification_verification__["a" /* VerificationPage */]);
    };
    FogotpasswordPage.prototype.signin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__signin_signin__["a" /* SigninPage */]);
    };
    FogotpasswordPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-fogotpassword',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\fogotpassword\fogotpassword.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title></ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <h4 text-center>Forgot Password?</h4>\n\n    <p text-center>Enter your email below to receive your password reset instructions</p>\n\n    <ion-list no-lines class="form row">\n\n        <ion-item>\n\n            <ion-input type="email" placeholder="Email address"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n    <button ion-button block (click)="verification()">Send</button>\n\n\n\n    <h3 text-center (click)="signin()">Back to login</h3>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\fogotpassword\fogotpassword.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */]])
    ], FogotpasswordPage);
    return FogotpasswordPage;
}());

//# sourceMappingURL=fogotpassword.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__verification_verification__ = __webpack_require__(138);




//import { ProfilePage } from '../../pages/profile/profile';
var RegisterPage = /** @class */ (function () {
    //public verificationId:string = "";
    //public verificationID:any;
    function RegisterPage(navCtrl, menuCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.menuCtrl.enable(false, 'myMenu');
    }
    //agent selection
    RegisterPage.prototype.agentSelected = function () {
        this.user = "agent";
        console.log(this.user);
    };
    //individual selection
    RegisterPage.prototype.individualSelected = function () {
        this.user = "individual";
        console.log(this.user);
    };
    RegisterPage.prototype.verification = function () {
        //verify phone number to send OTP
        console.log("verification, this.user->" + this.user);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__verification_verification__["a" /* VerificationPage */], { fullname: this.fullname, phonenumber: this.phonenumber, user: this.user });
    };
    RegisterPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\register\register.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title></ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n    <h4 text-center>New on Property Hub?</h4>\n\n    <p text-center>Registered with us in just few step</p>\n\n\n\n\n\n    <div class="register">\n\n        <ion-list no-lines radio-group class="form row">\n\n            <ion-list-header>\n\n                I am\n\n            </ion-list-header>\n\n            <ion-item col-6>\n\n                <ion-icon name="md-person" item-start></ion-icon>\n\n                <ion-label>Individual</ion-label>\n\n                <ion-radio checked="false" (ionSelect) = "individualSelected()" value="Individual" style="opacity: 0;"></ion-radio>\n\n            </ion-item>\n\n            <ion-item col-6>\n\n                <ion-icon name="md-home" item-start></ion-icon>\n\n                <ion-label>Agent</ion-label>\n\n                <ion-radio value="Agent" (ionSelect) = "agentSelected()" style="opacity: 0;"></ion-radio>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n\n\n        <ion-list no-lines class="form">\n\n            <ion-list-header>\n\n                Your mobile number\n\n            </ion-list-header>\n\n            <ion-item>\n\n                <ion-input type="tel" placeholder="Enter your 10 digit Number"  [(ngModel)] ="phonenumber"></ion-input>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n\n\n        <ion-list no-lines class="form">\n\n            <ion-list-header>\n\n                Your detail\n\n            </ion-list-header>\n\n            <ion-item>\n\n                <ion-input type="text" inputmode = "text" required = true placeholder="Full Name" [(ngModel)]="fullname" ></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-input type="email" inputmode ="email" placeholder="Email Address"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-input type="password"  required = true placeholder="Password"></ion-input>\n\n                <ion-buttons item-end>\n\n                    Show\n\n                </ion-buttons>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n    </div>\n\n    <button ion-button block (click)="verification()">Register</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\register\register.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(279);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_chat_chat__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_chatlist_chatlist__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_detail_detail__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_filters_filters__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_fogotpassword_fogotpassword__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_notification_notification__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_password_password__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_profileedit_profileedit__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_rateus_rateus__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_register_register__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_result_result__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_search_search__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_setting_setting__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_shortlisted_shortlisted__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_signin_signin__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_tnc_tnc__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_verification_verification__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_status_bar__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_splash_screen__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_list_list__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_data_data__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_camera__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_firebase__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_31_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__angular_http__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_home_service_home_service__ = __webpack_require__(437);































 //add firebase to webapp


// Initialize Firebase
__WEBPACK_IMPORTED_MODULE_31_firebase__["initializeApp"]({
    apiKey: "AIzaSyA_R-cGgAerrb4sCUOjIdhXtgs5d9E24VM",
    authDomain: "phone-auth-adbbb.firebaseapp.com",
    databaseURL: "https://phone-auth-adbbb.firebaseio.com",
    projectId: "phone-auth-adbbb",
    storageBucket: "phone-auth-adbbb.appspot.com",
    messagingSenderId: "1065717685153",
    appId: "1:1065717685153:web:e533c138e01ec7445209e0",
    measurementId: "G-VZJ5G84414"
});
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_chatlist_chatlist__["a" /* ChatlistPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_detail_detail__["a" /* DetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_filters_filters__["a" /* FiltersPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_fogotpassword_fogotpassword__["a" /* FogotpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profileedit_profileedit__["a" /* ProfileeditPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_rateus_rateus__["a" /* RateusPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_result_result__["a" /* ResultPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_shortlisted_shortlisted__["a" /* ShortlistedPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_signin_signin__["a" /* SigninPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tnc_tnc__["a" /* TncPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_list_list__["a" /* ListPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_32__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_chat_chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_chatlist_chatlist__["a" /* ChatlistPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_detail_detail__["a" /* DetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_filters_filters__["a" /* FiltersPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_fogotpassword_fogotpassword__["a" /* FogotpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profileedit_profileedit__["a" /* ProfileeditPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_rateus_rateus__["a" /* RateusPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_result_result__["a" /* ResultPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_shortlisted_shortlisted__["a" /* ShortlistedPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_signin_signin__["a" /* SigninPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tnc_tnc__["a" /* TncPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_list_list__["a" /* ListPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_camera__["a" /* Camera */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_29__providers_data_data__["a" /* DataProvider */],
                __WEBPACK_IMPORTED_MODULE_33__providers_home_service_home_service__["a" /* HomeServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_shortlisted_shortlisted__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_chatlist_chatlist__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_setting_setting__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tnc_tnc__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_about_about__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_contact_contact__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_rateus_rateus__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_signin_signin__ = __webpack_require__(137);















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_signin_signin__["a" /* SigninPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.home = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.profile = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */]);
    };
    MyApp.prototype.shortlisted = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_shortlisted_shortlisted__["a" /* ShortlistedPage */]);
    };
    MyApp.prototype.chatlist = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_chatlist_chatlist__["a" /* ChatlistPage */]);
    };
    MyApp.prototype.setting = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_setting_setting__["a" /* SettingPage */]);
    };
    MyApp.prototype.tnc = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_tnc_tnc__["a" /* TncPage */]);
    };
    MyApp.prototype.about = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_about_about__["a" /* AboutPage */]);
    };
    MyApp.prototype.contact = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_contact_contact__["a" /* ContactPage */]);
    };
    MyApp.prototype.rateus = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_rateus_rateus__["a" /* RateusPage */]);
    };
    MyApp.prototype.signin = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_signin_signin__["a" /* SigninPage */]);
    };
    Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Nav */]),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\app\app.html"*/'<ion-menu [content]="content" id="myMenu">\n\n    <ion-header menuClose (click)="profile()">\n\n        <ion-list no-margin no-lines>\n\n            <ion-item>\n\n                <ion-avatar item-start>\n\n                    <img src="assets/imgs/Layer30.png" />\n\n                </ion-avatar>\n\n                <h2>{{getfullname}}</h2>\n\n                <p>View Profile</p>\n\n            </ion-item>\n\n        </ion-list>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n        <ion-list no-margin no-lines>\n\n            <ion-item menuClose (click)="home()">\n\n                <ion-icon name="md-home" item-start></ion-icon>\n\n                Home\n\n            </ion-item>\n\n            <ion-item menuClose (click)="shortlisted()">\n\n                <ion-icon name="md-heart" item-start></ion-icon>\n\n                Shortlisted\n\n            </ion-item>\n\n            <ion-item menuClose (click)="chatlist()">\n\n                <ion-icon name="md-chatbubbles" item-start></ion-icon>\n\n                My Chat\n\n            </ion-item>\n\n            <ion-item menuClose (click)="setting()">\n\n                <ion-icon name="md-cog" item-start></ion-icon>\n\n                Settings\n\n            </ion-item>\n\n            <ion-item menuClose (click)="tnc()">\n\n                <ion-icon name="md-document" item-start></ion-icon>\n\n                Terms & condition\n\n            </ion-item>\n\n            <ion-item menuClose (click)="about()">\n\n                <ion-icon name="md-information-circle" item-start></ion-icon>\n\n                About us\n\n            </ion-item>\n\n            <ion-item menuClose (click)="contact()">\n\n                <ion-icon name="md-call" item-start></ion-icon>\n\n                Contact us\n\n            </ion-item>\n\n            <!--\n\n<ion-item menuClose (click)="rateus()">\n\n    <ion-icon name="md-thumbs-up" item-start></ion-icon>\n\n    Rate us\n\n</ion-item>\n\n-->\n\n            <ion-item menuClose (click)="signin()">\n\n                <ion-icon name="md-walk" item-start></ion-icon>\n\n                Sign Out\n\n            </ion-item>\n\n        </ion-list>\n\n        <p text-center> &copy; All right reserved | Opus Labs</p>\n\n    </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\app\app.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    var ListPage_1;
    ListPage = ListPage_1 = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-end>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\list\list.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);

//import { HttpClient } from '@angular/common/http';

var DataProvider = /** @class */ (function () {
    function DataProvider() {
        console.log('Hello DataProvider Provider');
    }
    DataProvider = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=data.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



/*
  Generated class for the HomeServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HomeServiceProvider = /** @class */ (function () {
    function HomeServiceProvider(http) {
        this.http = http;
        console.log('Hello HomeServiceProvider Provider');
    }
    //get form details - accessors
    HomeServiceProvider.prototype.getPropertyName = function () {
    };
    HomeServiceProvider.prototype.getPropertyLocation = function () {
    };
    HomeServiceProvider.prototype.getPropertyPrice = function () {
    };
    HomeServiceProvider.prototype.getPropertyDescription = function () {
    };
    HomeServiceProvider = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], HomeServiceProvider);
    return HomeServiceProvider;
}());

//# sourceMappingURL=home-service.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(130);




var ChatlistPage = /** @class */ (function () {
    function ChatlistPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ChatlistPage.prototype.chat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */]);
    };
    ChatlistPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-chatlist',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\chatlist\chatlist.html"*/'<ion-header class="bg-theme">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Messages</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <ion-list no-lines>\n\n        <ion-item (click)="chat()" class="unread">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n                <ion-badge item-end>2</ion-badge>\n\n            </ion-avatar>\n\n            <h2>Jhonson Smith</h2>\n\n            <p>Sorry to say Price is not negotiable</p>\n\n            <ion-note item-end>Just now</ion-note>\n\n        </ion-item>\n\n        <ion-item (click)="chat()" class="unread">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n                <ion-badge item-end>2</ion-badge>\n\n            </ion-avatar>\n\n            <h2>Emili Willams</h2>\n\n            <p>This town ain\'t big enough for the two of us!</p>\n\n            <ion-note item-end>5 mint ago</ion-note>\n\n        </ion-item>\n\n        <ion-item (click)="chat()">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n            </ion-avatar>\n\n            <h2>Jhonson Smith</h2>\n\n            <p>This town ain\'t big enough for the two of us!</p>\n\n            <ion-note item-end>12:23pm</ion-note>\n\n        </ion-item>\n\n        <ion-item (click)="chat()">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n            </ion-avatar>\n\n            <h2>Emili Willams</h2>\n\n            <p>This town ain\'t big enough for the two of us!</p>\n\n            <ion-note item-end>12:23am</ion-note>\n\n        </ion-item>\n\n        <ion-item (click)="chat()">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n            </ion-avatar>\n\n            <h2>Jhonson Smith</h2>\n\n            <p>This town ain\'t big enough for the two of us!</p>\n\n            <ion-note item-end>Today</ion-note>\n\n        </ion-item>\n\n        <ion-item (click)="chat()">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/Layer30.png">\n\n            </ion-avatar>\n\n            <h2>Emili Willams</h2>\n\n            <p>This town ain\'t big enough for the two of us!</p>\n\n            <ion-note item-end>04-Oct-18</ion-note>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\chatlist\chatlist.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], ChatlistPage);
    return ChatlistPage;
}());

//# sourceMappingURL=chatlist.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shortlisted_shortlisted__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contact_contact__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chat_chat__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rateus_rateus__ = __webpack_require__(131);







var DetailPage = /** @class */ (function () {
    function DetailPage(navCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.data = [
            {
                title: "Railway Station (2)",
                details1: "Worli Railway Station",
                details2: "9 min | 2.7 km",
                details3: "Chinchpokli Station",
                details4: "15 min | 5.7 km",
            },
            {
                title: "Airport (1)",
                details1: "Shiwagi Terminal Airport",
                details2: "9 min | 2.7 km",
            },
            {
                title: "Hospitals (3)",
                details1: "Agrsen Hospital",
                details2: "9 min | 2.7 km",
                details3: "AIMS Hospital",
                details4: "15 min | 5.7 km",
                details5: "Banwari Hospital",
                details6: "16 min | 6 km",
            },
            {
                title: "Banks (1)",
                details1: "SBI Station",
                details2: "1 min | 500 m",
            }
        ];
    }
    DetailPage.prototype.shortlisted = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__shortlisted_shortlisted__["a" /* ShortlistedPage */]);
    };
    DetailPage.prototype.contact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__contact_contact__["a" /* ContactPage */]);
    };
    DetailPage.prototype.chat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chat_chat__["a" /* ChatPage */]);
    };
    DetailPage.prototype.rateus = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__rateus_rateus__["a" /* RateusPage */]);
        modal.present();
    };
    DetailPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
        }
        else {
            data.showDetails = true;
        }
    };
    DetailPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-detail',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\detail\detail.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title></ion-title>\n\n        <ion-buttons end margin-left margin-right>\n\n            <button ion-button clear (click)="shortlisted()">\n\n                <ion-icon name="md-heart-outline"></ion-icon>\n\n            </button>\n\n            <button ion-button clear>\n\n                <ion-icon name="md-share"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="bg-light">\n\n    <div class="header-img">\n\n        <img src="assets/imgs/Layer4.png">\n\n        <ion-icon name="md-star" (click)="rateus()"></ion-icon>\n\n    </div>\n\n    <ion-list no-lines class="few-details">\n\n        <ion-item>\n\n            <h1>Alise Altezza</h1>\n\n            <p>Near worli sea Link,Worli, Mumbai.</p>\n\n            <button ion-button clear item-end>$750k</button>\n\n        </ion-item>\n\n        <ion-item>\n\n            <div item-start>\n\n                <p>Area(sq.fit)</p>\n\n                <h2>4500</h2>\n\n            </div>\n\n            <div item-end>\n\n                <p>Configuration</p>\n\n                <h2>3 Bed & 3 Bathrooms</h2>\n\n            </div>\n\n        </ion-item>\n\n    </ion-list>\n\n    <div class="prduucts-detail">\n\n        <p>About Alise Altezza</p>\n\n        Ionic apps are made of high-level building blocks called components. Components allow you to quickly construct\n\n        an interface for your app. Ionic comes with a number of components, including modals, popups, and cards. Check\n\n        out the examples below to see what each component looks like and to learn how to use each one.<br><br>\n\n    </div>\n\n    <div class="prduucts-detail">\n\n        <p>Project Amenities</p>\n\n        <ul>\n\n            <li>Landscape Gardan</li>\n\n            <li>Jogging Track</li>\n\n            <li>Power Backup</li>\n\n            <li>Complete RCC Structure</li>\n\n            <li>Designer Door Frames</li>\n\n            <li>PVC Concealed wiring</li>\n\n        </ul>\n\n    </div>\n\n    <img src="assets/imgs/map.png">\n\n    <div class="other-detail">\n\n        <h5><small>Neighbourhood</small>View in Map</h5>\n\n        <ion-list no-lines>\n\n            <ion-item *ngFor="let d of data">\n\n                <h3>{{d.title}}</h3>\n\n                <ion-note item-end (click)="toggleDetails(d)">+</ion-note>\n\n                <div *ngIf="d.showDetails">\n\n                    <p>{{d.details1}}<span>{{d.details2}}</span></p>\n\n                    <p>{{d.details3}}<span>{{d.details4}}</span></p>\n\n                    <p>{{d.details5}}<span>{{d.details6}}</span></p>\n\n                </div>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n    <div class="posted">\n\n        <ion-list no-lines>\n\n            <ion-list-header>Posted by</ion-list-header>\n\n            <ion-item>\n\n                <ion-avatar item-start>\n\n                    <img src="assets/imgs/Layer30.png">\n\n                </ion-avatar>\n\n                <h2>John Doe</h2>\n\n                <p>Owner</p>\n\n                <button ion-button item-end icon-start small (click)="chat()">\n\n                    <ion-icon name="md-chatbubbles"></ion-icon>Chat\n\n                </button>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\detail\detail.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */]])
    ], DetailPage);
    return DetailPage;
}());

//# sourceMappingURL=detail.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);



var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\contact\contact.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>Contact us</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <div class="header-logo" text-center>\n\n        <figure>\n\n            <img src="assets/imgs/logo.png">\n\n        </figure>\n\n        <p>Have any question or comments? Let us know.</p>\n\n    </div>\n\n    <ion-list no-lines class="form" style="padding-bottom: 0;">\n\n        <ion-item>\n\n            <ion-input type="text" placeholder="Your Name"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-input type="text" placeholder="Email or Phone Number"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-textarea placeholder="Your Message" style="height: 8rem;"></ion-textarea>\n\n        </ion-item>\n\n    </ion-list>\n\n    <div class="form" style="padding-bottom: 0;padding-top: 0;">\n\n        <button ion-button block>Send</button>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\contact\contact.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tslib__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_search__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__notification_notification__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chatlist_chatlist__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detail_detail__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_operators__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_operators__);










var HomePage = /** @class */ (function () {
    function HomePage(http, navParams, loadingCtrl, camera, navCtrl, menuCtrl, alertCtrl) {
        this.http = http;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.option = "buy";
        this.menuCtrl.enable(true, 'myMenu');
        this.showView = navParams.get('user');
        console.log("this.showView->" + this.showView);
    }
    HomePage.prototype.search = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__search_search__["a" /* SearchPage */]);
    };
    HomePage.prototype.notification = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__notification_notification__["a" /* NotificationPage */]);
    };
    HomePage.prototype.chatlist = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chatlist_chatlist__["a" /* ChatlistPage */]);
    };
    HomePage.prototype.detail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__detail_detail__["a" /* DetailPage */]);
    };
    HomePage.prototype.accessGallery = function () {
        var _this = this;
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            destinationType: this.camera.DestinationType.DATA_URL
        }).then(function (imageData) {
            _this.base64Image = "data:image/jpeg;base64," + imageData;
            _this.picture = imageData;
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.onSubmit = function (form) {
        alert("Hello " + "Nice");
        console.log("description -> " + this.description); //debug values sent from form
        console.log("propertyName ->" + this.propertyName);
        console.log("location -> " + this.location);
        console.log("price -> " + this.price);
    };
    HomePage.prototype.upload = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]();
        headers.append("Accept", 'application/json');
        headers.append("Content-Type", 'application/json');
        var options = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var data = {
            image: this.base64Image
        };
        var loader = this.loadingCtrl.create({
            content: "Processing please wait…",
        });
        loader.present().then(function () {
            _this.http.post("http://ionicdon.com/mobile/upload_data.php", data, options)
                .pipe(Object(__WEBPACK_IMPORTED_MODULE_9_rxjs_operators__["map"])(function (res) { return res.json(); }))
                .subscribe(function (res) {
                loader.dismiss();
                if (res == "Successfully_Uploaded") {
                    var alert_1 = _this.alertCtrl.create({
                        title: "CONGRATS",
                        subTitle: (res),
                        buttons: ["OK"]
                    });
                    alert_1.present();
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: "ERROR",
                        subTitle: "Image could not be uploaded",
                        buttons: ["OK"]
                    });
                    alert_2.present();
                }
            });
        });
    };
    HomePage = Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__decorate"])([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\home\home.html"*/'<!-- showview true then AgentFormBlock else IndividualBlock !-->\n\n<div *ngIf ="showView == \'agent\'; then AgentFormBlock; else IndividualBlock"></div>\n\n\n\n  <!-- view for Individual -->\n\n  <ng-template #IndividualBlock>\n\n    <ion-header class="bg-home">\n\n       <ion-navbar>\n\n           <button ion-button menuToggle>\n\n         <ion-icon name="menu"></ion-icon>\n\n       </button>\n\n           <ion-title>AGY-AMBALA</ion-title>\n\n           <ion-buttons end>\n\n               <button ion-button clear (click)="chatlist()">\n\n               <ion-icon name="md-chatbubbles"></ion-icon>\n\n               </button>\n\n\n\n               <button ion-button clear (click)="notification()">\n\n                   <ion-icon name="md-notifications"></ion-icon>\n\n               </button>\n\n           </ion-buttons>\n\n       </ion-navbar>\n\n\n\n       <p text-center>Real Estate</p>\n\n       <h1 text-center>Made Simple</h1>\n\n       <div class="option">\n\n           <ion-segment [(ngModel)]="option">\n\n               <ion-segment-button value="buy">\n\n                   Buy\n\n               </ion-segment-button>\n\n               <ion-segment-button value="rent">\n\n                   Rent\n\n               </ion-segment-button>\n\n               <!-- <ion-segment-button value="sell">\n\n                   Sell\n\n               </ion-segment-button> -->\n\n           </ion-segment>\n\n           <ion-searchbar (click)="search()" placeholder="Search by location, property, landmark, etc..."></ion-searchbar>\n\n       </div>\n\n   </ion-header>\n\n\n\n   <ion-content padding-top class="bg-light">\n\n       <h6 padding>Featured Properties</h6>\n\n       <div [ngSwitch]="option">\n\n           <!-- <ion-list *ngSwitchCase="\'buy\'">\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n           </ion-list> -->\n\n\n\n           <ion-list *ngSwitchCase="\'rent\'">\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               {{propertyName}}<br>\n\n                               <small>{{description}}</small>\n\n                               <ion-buttons item-end text-right>GHC{{price}}<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <!-- <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card> -->\n\n           </ion-list>\n\n           <!-- <ion-list *ngSwitchCase="\'sell\'">\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n               <ion-card class="items-div" (click)="detail()">\n\n                   <ion-icon name="md-heart" class="like"></ion-icon>\n\n                   <ion-scroll scrollX class="scrol-items-div">\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer4.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer2.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer23.png" />\n\n                       </div>\n\n                       <div class="scroll-item">\n\n                           <img src="assets/imgs/Layer6.png" />\n\n                       </div>\n\n                   </ion-scroll>\n\n\n\n                   <ion-card-content>\n\n                       <ion-list>\n\n                           <button ion-item no-padding>\n\n                               Alise Altezza<br>\n\n                               <small>Near worli sea link, worli, Mumbai.</small>\n\n                               <ion-buttons item-end text-right>$750K<br><small>3BHK | 4500sq.ft</small></ion-buttons>\n\n                           </button>\n\n                       </ion-list>\n\n                   </ion-card-content>\n\n               </ion-card>\n\n           </ion-list> -->\n\n       </div>\n\n\n\n   </ion-content>\n\n  </ng-template>\n\n\n\n  <!-- Form for agent -->\n\n  <ng-template #AgentFormBlock>\n\n    <ion-header>\n\n      <ion-title>AGY AMBALA REAL ESTATE FORM</ion-title>\n\n    </ion-header>\n\n    <ion-content>\n\n    <form #form="ngForm" (ngSubmit) = "onSubmit(form)">\n\n         <ion-item>\n\n           <ion-label>Property Information</ion-label>\n\n         </ion-item>\n\n         <ion-item>\n\n           <ion-label floating>Property Name:</ion-label>\n\n           <ion-input type="text" name="propertyName" [(ngModel)]= "propertyName" [ngModelOptions]="{standalone: true}"></ion-input>\n\n         </ion-item>\n\n         <ion-item>\n\n           <ion-label floating>Location:</ion-label>\n\n           <ion-input type="text" name = "location" [(ngModel)]= "location"  [ngModelOptions]="{standalone: true}" ></ion-input>\n\n         </ion-item>\n\n\n\n           <ion-list>\n\n      <ion-list-header>\n\n        Type of Building:\n\n      </ion-list-header>\n\n        <ion-item>\n\n          <ion-label>Apartment</ion-label>\n\n          <ion-checkbox color="dark" checked="false"></ion-checkbox>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>Compound House</ion-label>\n\n          <ion-checkbox color="dark" checked="false"></ion-checkbox>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>Self Compound House</ion-label>\n\n          <ion-checkbox color="dark" checked="false"></ion-checkbox>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>Apartment</ion-label>\n\n          <ion-checkbox color="dark" checked="false"></ion-checkbox>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>Price</ion-label>\n\n            <ion-input type="number" name = "price" [(ngModel)] = "price" ></ion-input>\n\n          </ion-item>\n\n\n\n         <ion-item>\n\n           <ion-label>Description</ion-label>\n\n           <ion-textarea name="description" [(ngModel)] = "description" [ngModelOptions]="{standalone: true}"></ion-textarea>\n\n         </ion-item>\n\n         </ion-list>\n\n\n\n          <!-- <button ion-button large  outline (click)="accessGallery()">Acesss Property Gallery</button>\n\n         <img [src]="base64Image" *ngIf="base64Image" />\n\n         <button ion-button full (click)= "upload()"  *ngIf="base64Image">Upload</button>\n\n        <button ion-button type="submit" block>List Property</button> -->\n\n        <button type="submit" class="btn btn-success" >Submit</button>   \n\n    </form>\n\n    </ion-content>\n\n  </ng-template>\n\n'/*ion-inline-end:"C:\Users\Samuel\Desktop\Maame\payment\src\pages\home\home.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0_tslib__["__metadata"])("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[269]);
//# sourceMappingURL=main.js.map