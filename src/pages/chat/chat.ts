import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage {
  private detail = false;
  
  constructor(public navCtrl: NavController) {
  }

  showDetail() {
    this.detail = !this.detail;
    console.log(this.detail);
  }

}