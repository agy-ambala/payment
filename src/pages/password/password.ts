import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { FogotpasswordPage } from '../fogotpassword/fogotpassword';

@Component({
  selector: 'page-password',
  templateUrl: 'password.html'
})
export class PasswordPage {

  
 constructor(public navCtrl: NavController, public menuCtrl: MenuController ) {
  this.menuCtrl.enable(false, 'myMenu');
  }
  
  home() {
    this.navCtrl.setRoot(HomePage);
  }
  
  fogotpassword() {
    this.navCtrl.push(FogotpasswordPage);
  }

}
