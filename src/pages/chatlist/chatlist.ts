import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';

@Component({
  selector: 'page-chatlist',
  templateUrl: 'chatlist.html'
})
export class ChatlistPage {

  constructor(public navCtrl: NavController) {

  }
  
    chat() {
    this.navCtrl.push(ChatPage);
  }  

}