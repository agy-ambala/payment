import { Component } from '@angular/core';
import { NavController, MenuController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import * as firebase from 'firebase';
declare var window;

@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html'
})
export class VerificationPage {

  verificationId:string | null;
  code: string = "";
  //credential:any;

   public fullname:string;
   public phonenumber:any;

   public user:string;

   //OTP values
   public value1: string = "";
   public value2: string = "";
   public value3: string = "";
   public value4: string = "";
   public value5: string = "";
   public value6: string = "";
  //let verificationId;
  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public navParams: NavParams

  ) {
  this.menuCtrl.enable(false, 'myMenu');
  this.fullname = navParams.get('fullname');
  this.phonenumber=navParams.get('phonenumber');
  this.user = navParams.get('user');
  console.log("this.user->"+this.user);

  //verify phone number
  (<any>window).FirebasePlugin.verifyPhoneNumber(this.phonenumber, 60, (credential) => {

    //sms sent checked
    console.log("credential:" + credential);

    // console.log(credential);
     //this.code = credential.instantVerification ? credential.code : inputField.value.toString();
    console.log("credential.verificationId->"+credential.verificationId);
    var verificationIdn = credential.verificationId;
    this.verificationId = verificationIdn;
    console.log("this.verificationId ->" + this.verificationId);


 //this.navCtrl.push(VerificationPage,{fullname:this.fullname,phonenumber:this.phonenumber});

   }, error => {
     console.log("**error:**" + error);
   });
  //this.verificationId = navParams.get('vid');
  }


home(value1:HTMLInputElement, value2:HTMLInputElement, value3:HTMLInputElement, value4:HTMLInputElement, value5:HTMLInputElement,value6:HTMLInputElement){

this.value1 = value1.value;
this.value2 = value2.value;
this.value3 = value3.value;
this.value4 = value4.value;
this.value5 = value5.value;
this.value6 = value6.value;

  console.log("this.value1->", this.value1);
  console.log("this.value2->", this.value2);
  console.log("this.value3->", this.value3);
  console.log("this.value4->", this.value4);
  console.log("this.value5->", this.value5);
  console.log("this.value6->", this.value6);


  this.code = this.value1 + this.value2 + this.value3 + this.value4 + this.value5 + this.value6;
  console.log("this.code->" + this.code);

  let cred = firebase.auth.PhoneAuthProvider.credential(this.verificationId, this.code);

  firebase.auth().signInWithCredential(cred).then(() => {

    //navigate to homepage for successful login
      this.navCtrl.setRoot(HomePage,{user:this.user});

  }, reason => {
     console.log(reason);
  });

  //
  // (<any>window).FirebasePlugin.verifyPhoneNumber(this.phonenumber, 60, function(credential) {
  //
  //   //sms sent checked
  //   console.log("credential:" + credential);
  //
  //   // console.log(credential);
  //    //this.code = credential.instantVerification ? credential.code : inputField.value.toString();
  //   console.log("credential.verificationId->"+credential.verificationId);
  //   let verificationId = credential.verificationId;
  //   console.log("this.verificationId ->" + verificationId);
  //
  //
  //
  //   let cred = firebase.auth.PhoneAuthProvider.credential(verificationId, this.code);
  //
  //   //window.prompt('credential ->' + credential);
  //
  //   firebase.auth().signInWithCredential(cred).then(() => {
  //
  //     //navigate to homepage for successful login
  //       this.navCtrl.setRoot(HomePage);
  //
  //   }, reason => {
  //      console.log(reason);
  //   });
  //
  //   // sign in with the credential
  //   //firebase.auth().signInWithCredential(credential);
  //
  //   // this.verificationID = credential.verificationId ; //save credential id
  //  }, error => {
  //    console.log("**error:**" + error);
  //  });
  //
  //



//this.navCtrl.setRoot(HomePage);



}







}
