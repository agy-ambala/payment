import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { ShortlistedPage } from '../shortlisted/shortlisted';
import { ContactPage } from '../contact/contact';
import { ChatPage } from '../chat/chat';
import { RateusPage } from '../rateus/rateus';

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html'
})
export class DetailPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }

  shortlisted() {
    this.navCtrl.push(ShortlistedPage);
  }
  contact() {
    this.navCtrl.push(ContactPage);
  }
  chat() {
    this.navCtrl.push(ChatPage);
  }


  rateus() {
    const modal = this.modalCtrl.create(RateusPage);
    modal.present();
  }

  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
    } else {
      data.showDetails = true;
    }
  }


  data = [
    {
      title: "Railway Station (2)",
      details1: "Worli Railway Station",
      details2: "9 min | 2.7 km",
      details3: "Chinchpokli Station",
      details4: "15 min | 5.7 km",
    },
    {
      title: "Airport (1)",
      details1: "Shiwagi Terminal Airport",
      details2: "9 min | 2.7 km",
    },
    {
      title: "Hospitals (3)",
      details1: "Agrsen Hospital",
      details2: "9 min | 2.7 km",
      details3: "AIMS Hospital",
      details4: "15 min | 5.7 km",
      details5: "Banwari Hospital",
      details6: "16 min | 6 km",
    },
    {
      title: "Banks (1)",
      details1: "SBI Station",
      details2: "1 min | 500 m",
    }
  ];

}