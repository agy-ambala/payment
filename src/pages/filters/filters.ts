import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html'
})
export class FiltersPage {

 structure: any = { lower: 33, upper: 60 };
 gaming1: string = "p1";
 gaming2: string = "b1";
 gaming3: string = "b5";

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {

  }
  
  
 // close() {
 //   this.viewCtrl.dismiss();
//}

}
