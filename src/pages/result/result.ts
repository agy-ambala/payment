import { Component } from '@angular/core';
import { NavController, ModalController, ActionSheetController } from 'ionic-angular';

import { FiltersPage } from '../filters/filters';
import { ChatlistPage } from '../chatlist/chatlist';
import { DetailPage } from '../detail/detail';

@Component({
  selector: 'page-result',
  templateUrl: 'result.html'
})
export class ResultPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public actionsheetCtrl: ActionSheetController) {

  }
  

  
   //filters() {
   // const modal = this.modalCtrl.create(FiltersPage);
   // modal.present();
  //}
        filters() {
    this.navCtrl.push(FiltersPage);
  }
         chatlist() {
    this.navCtrl.push(ChatlistPage);
  }
        detail() {
    this.navCtrl.push(DetailPage);
  }
  
    openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Sort By',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Relevance',
         // role: 'radio',
         // handler: () => {
         //   console.log('Favorite clicked');
         // }
        },
        {
          text: 'Most Recent',
        },
        {
          text: 'Price: Low to High',
        },
        {
          text: 'Price: Low to High',
       
        }
      ]
    });
      actionSheet.present();
}

}
