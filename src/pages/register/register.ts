import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { VerificationPage } from '../verification/verification';

//import { ProfilePage } from '../../pages/profile/profile';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

 //data members
  public fullname:string; //save name from register page
  public phonenumber:any; //save phonenumber from register page
  public user:string;
  //public verificationId:string = "";
  //public verificationID:any;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController ) {
  this.menuCtrl.enable(false, 'myMenu');
}


//agent selection
agentSelected()
{
  this.user = "agent";
  console.log(this.user);

}

//individual selection
individualSelected()
{
  this.user = "individual";
  console.log(this.user);

}


  verification() {
      //verify phone number to send OTP
      console.log("verification, this.user->"+this.user);
    this.navCtrl.push(VerificationPage,{fullname:this.fullname,phonenumber:this.phonenumber, user:this.user});
  }


}
