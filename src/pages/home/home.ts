import { Component } from '@angular/core';
import { NavController, MenuController, AlertController,LoadingController, NavParams  } from 'ionic-angular';

import { SearchPage } from '../search/search';
import { NotificationPage } from '../notification/notification';
import { ChatlistPage } from '../chatlist/chatlist';
import { DetailPage } from '../detail/detail';
import { Camera } from '@ionic-native/camera';
import {Http ,RequestOptions, Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

option: string = "buy";
public base64Image: string;
public picture: string;

public showView: string; //when true AgentFormBlock else IndividualBlock

   constructor(public http:Http,public navParams: NavParams, public loadingCtrl:LoadingController, public camera:Camera, public navCtrl: NavController, public menuCtrl: MenuController, public alertCtrl: AlertController ) {
  this.menuCtrl.enable(true, 'myMenu');
  this.showView = navParams.get('user');
  console.log("this.showView->"+this.showView);
  }

      search() {
    this.navCtrl.push(SearchPage);
  }
      notification() {
    this.navCtrl.push(NotificationPage);
  }
      chatlist() {
    this.navCtrl.push(ChatlistPage);
  }
      detail() {
    this.navCtrl.push(DetailPage);
  }

  accessGallery(){

   this.camera.getPicture({

    sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,

    destinationType: this.camera.DestinationType.DATA_URL

   }).then((imageData) => {

     this.base64Image = "data:image/jpeg;base64,"+imageData;

     this.picture = imageData;

        }, (err) => {

     console.log(err);

   });

}


   upload(){

  var headers = new Headers();

  headers.append("Accept", 'application/json');

  headers.append("Content-Type", 'application/json' );

  let options = new RequestOptions({ headers: headers });

    let data = {

      image:this.base64Image

    };

  let loader = this.loadingCtrl.create({

  content: "Processing please wait…",

});

loader.present().then(() => {

this.http.post("http://ionicdon.com/mobile/upload_data.php",data,options)

.pipe(map(res => res.json()))

.subscribe(res => {

 loader.dismiss()

 if(res=="Successfully_Uploaded"){

   let alert = this.alertCtrl.create({

     title:"CONGRATS",

    subTitle:(res),

    buttons: ["OK"]

     });

         alert.present();

 }else {

  let alert = this.alertCtrl.create({

  title:"ERROR",

  subTitle:"Image could not be uploaded",

  buttons: ["OK"]

  });

  alert.present();

   }

 });

 });

  }

}
