import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { ChatlistPage } from '../chatlist/chatlist';

@Component({
  selector: 'page-shortlisted',
  templateUrl: 'shortlisted.html'
})
export class ShortlistedPage {

  constructor(public navCtrl: NavController) {

  }
        detail() {
    this.navCtrl.push(DetailPage);
  }
    chatlist() {
    this.navCtrl.push(ChatlistPage);
  }

}
