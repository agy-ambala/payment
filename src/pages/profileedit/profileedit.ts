import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-profileedit',
  templateUrl: 'profileedit.html'
})
export class ProfileeditPage {

  constructor(public navCtrl: NavController) {

  }

}
