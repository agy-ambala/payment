import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-rateus',
  templateUrl: 'rateus.html'
})
export class RateusPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {

  }
  
 close() {
   this.viewCtrl.dismiss();
}

}