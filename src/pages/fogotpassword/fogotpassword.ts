import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { VerificationPage } from '../verification/verification';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-fogotpassword',
  templateUrl: 'fogotpassword.html'
})
export class FogotpasswordPage {

   constructor(public navCtrl: NavController, public menuCtrl: MenuController ) {
  this.menuCtrl.enable(false, 'myMenu');
  }
    verification() {
    this.navCtrl.push(VerificationPage);
  }
    signin() {
    this.navCtrl.setRoot(SigninPage);
  }
}
