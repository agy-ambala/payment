import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { PasswordPage } from '../password/password';
import { RegisterPage } from '../register/register';


@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {

  constructor(public navCtrl: NavController, public menuCtrl: MenuController ) {
  this.menuCtrl.enable(false, 'myMenu');
  }
  
   password() {
    this.navCtrl.push(PasswordPage);
  }
    register() {
    this.navCtrl.push(RegisterPage);
  }

}
