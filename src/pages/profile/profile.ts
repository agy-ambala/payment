import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { ProfileeditPage } from '../profileedit/profileedit';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {



  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
  }

      profileedit() {
    this.navCtrl.push(ProfileeditPage);
  }

}
