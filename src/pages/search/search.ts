import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ResultPage } from '../result/result';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  constructor(public navCtrl: NavController) {

  }

    result() {
    this.navCtrl.push(ResultPage);
  }

}
