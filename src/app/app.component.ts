import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { ShortlistedPage } from '../pages/shortlisted/shortlisted';
import { ChatlistPage } from '../pages/chatlist/chatlist';
import { SettingPage } from '../pages/setting/setting';
import { TncPage } from '../pages/tnc/tnc';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { RateusPage } from '../pages/rateus/rateus';
import { ProfilePage } from '../pages/profile/profile';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SigninPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  
  
 home() {
    this.nav.setRoot(HomePage);
  }  
    profile() {
    this.nav.setRoot(ProfilePage);
  }
  shortlisted() {
    this.nav.setRoot(ShortlistedPage);
  }
   
  chatlist() {
    this.nav.setRoot(ChatlistPage);
  }   
  
  setting() {
    this.nav.setRoot(SettingPage);
  }
    
  tnc() {
    this.nav.setRoot(TncPage);
  }
      
  about() {
    this.nav.setRoot(AboutPage);
  }
        
  contact() {
    this.nav.setRoot(ContactPage);
  }     
  
  rateus() {
    this.nav.setRoot(RateusPage);
  }   
  
  signin() {
    this.nav.setRoot(SigninPage);
  }
  
  
  
}
