import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ChatPage } from '../pages/chat/chat';
import { ChatlistPage } from '../pages/chatlist/chatlist';
import { ContactPage } from '../pages/contact/contact';
import { DetailPage } from '../pages/detail/detail';
import { FiltersPage } from '../pages/filters/filters';
import { FogotpasswordPage } from '../pages/fogotpassword/fogotpassword';
import { HomePage } from '../pages/home/home';
import { NotificationPage } from '../pages/notification/notification';
import { PasswordPage } from '../pages/password/password';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileeditPage } from '../pages/profileedit/profileedit';
import { RateusPage } from '../pages/rateus/rateus';
import { RegisterPage } from '../pages/register/register';
import { ResultPage } from '../pages/result/result';
import { SearchPage } from '../pages/search/search';
import { SettingPage } from '../pages/setting/setting';
import { ShortlistedPage } from '../pages/shortlisted/shortlisted';
import { SigninPage } from '../pages/signin/signin';
import { TncPage } from '../pages/tnc/tnc';
import { VerificationPage } from '../pages/verification/verification';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ListPage } from '../pages/list/list';
import { DataProvider } from '../providers/data/data';
import { Camera } from '@ionic-native/camera';
import * as firebase from 'firebase';  //add firebase to webapp
import { HttpModule } from '@angular/http';

  // Initialize Firebase
firebase.initializeApp({
    apiKey: "AIzaSyA_R-cGgAerrb4sCUOjIdhXtgs5d9E24VM",
    authDomain: "phone-auth-adbbb.firebaseapp.com",
    databaseURL: "https://phone-auth-adbbb.firebaseio.com",
    projectId: "phone-auth-adbbb",
    storageBucket: "phone-auth-adbbb.appspot.com",
    messagingSenderId: "1065717685153",
    appId: "1:1065717685153:web:e533c138e01ec7445209e0",
    measurementId: "G-VZJ5G84414"
  });




@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ChatPage,
    ChatlistPage,
    ContactPage,
    DetailPage,
    FiltersPage,
    FogotpasswordPage,
    HomePage,
    NotificationPage,
    PasswordPage,
    ProfilePage,
    ProfileeditPage,
    RateusPage,
    RegisterPage,
    ResultPage,
    SearchPage,
    SettingPage,
    ShortlistedPage,
    SigninPage,
    TncPage,
    VerificationPage,
    ListPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ChatPage,
    ChatlistPage,
    ContactPage,
    DetailPage,
    FiltersPage,
    FogotpasswordPage,
    HomePage,
    NotificationPage,
    PasswordPage,
    ProfilePage,
    ProfileeditPage,
    RateusPage,
    RegisterPage,
    ResultPage,
    SearchPage,
    SettingPage,
    ShortlistedPage,
    SigninPage,
    TncPage,
    VerificationPage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider
  ]
})
export class AppModule {}
